# Tests techniques I2H

## One for All

> J'ai créé ce projet depuis le Vue-CLI 3. J'ai fusionné les deux exercices afin de centraliser ma candidature.
> Je ne connaissais pas le Framwork Quasar, j'ai donc essayé de faire de mon mieux.

## Technologies
* ES6
* Vue.js
* vue-router
* ESLint ( AirBnB )
* SCSS
* Quasar Framework
* Webpack

## Project setup
```
npm install
```
Compiles and hot-reloads for development :

```
npm run serve
```


