import 'quasar-extras/material-icons';
import Quasar from 'quasar';
import Vue from 'vue';
import App from './App.vue';
import router from './router';


import './styles/quasar.styl';


Vue.use(Quasar, {
  config: {},
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
