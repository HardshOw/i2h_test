import Vue from 'vue';
import Router from 'vue-router';
import DefaultLayout from './layouts/Default.vue';
import Home from './views/Home.vue';
import Integration from './views/Integration.vue';
import Maquette from './views/Maquette.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      component: DefaultLayout,
      children: [
        {
          path: '',
          name: 'home',
          component: Home,
        },
        {
          path: '/integration',
          name: 'integration',
          component: Integration,
        },
        {
          path: '/maquette',
          name: 'maquette',
          component: Maquette,
        },
      ],
    },
  ],
});
